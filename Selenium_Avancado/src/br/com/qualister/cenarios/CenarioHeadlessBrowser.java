package br.com.qualister.cenarios;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;

public class CenarioHeadlessBrowser {

	private WebDriver driver;

	@Before
	public void iniciaTeste() {
		DesiredCapabilities dc = new DesiredCapabilities();
		dc.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "c:\\phantomjs.exe");
		dc.setCapability("takeScreenshot", true);
		dc.setJavascriptEnabled(true);

		driver = new PhantomJSDriver(dc);
		driver.get("http://eliasnogueira.com/selenium/exercicios/brasil/webdriver/avancado");
		driver.findElement(By.partialLinkText("Headless Browser")).click();
	}

	@After
	public void finalizaTeste() {
		driver.quit();
	}

	@Test
	public void validarCadastro() throws IOException {
		driver.findElement(By.id("nome")).sendKeys("Jose");
		driver.findElement(By.id("sobrenome")).sendKeys("Silva");
		driver.findElement(By.id("email")).sendKeys("jose.silva@gmail.com");

		File arquivo = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(arquivo, new File("evidencias/tela1.png"));

		driver.findElement(By.id("enviar")).click();

		arquivo = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(arquivo, new File("evidencias/tela2.png"));

		Assert.assertEquals(driver.findElement(By.id("nome")).getText(), "Ol�, Jose Silva");
		Assert.assertEquals(driver.findElement(By.id("email")).getText(), "O email digitado foi jose.silva@gmail.com");
	}
}
