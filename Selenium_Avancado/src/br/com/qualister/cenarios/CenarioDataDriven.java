package br.com.qualister.cenarios;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

@RunWith(Parameterized.class)
public class CenarioDataDriven {

	@Before
	public void iniciaTeste() {
		driver = new FirefoxDriver();
		driver.get("http://eliasnogueira.com/selenium/exercicios/brasil/webdriver/avancado");
		driver.findElement(By.partialLinkText("Login Data Driven")).click();
		wait = new WebDriverWait(driver, 15);
	}

	@After
	public void finalizaTeste() {
		driver.quit();
	}
	
	private WebDriver driver;
	private WebDriverWait wait;

	private String usuario;
	private int id;

	public CenarioDataDriven(String usuario, int id) {
		this.usuario = usuario;
		this.id = id;
	}
	
	@Parameters
	public static List<Object[]> lista() {
		return Arrays.asList(new Object[][] {
				{ "usuario01", 1 },
				{ "usuario02", 2 },
				{ "usuario03", 3 },
				{ "usuario04", 4 }
			}
		);
	}

	@Test
	public void validaLogin() {
		driver.findElement(By.id("login")).sendKeys(usuario);
		driver.findElement(By.id("senha")).sendKeys(usuario);
		driver.findElement(By.xpath("//input[@value='Entrar']")).click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("loader")));
		Assert.assertEquals(driver.findElement(By.id("usuario")).getText(), "Bem vindo Usuario 0" + id);
		Assert.assertEquals(driver.findElement(By.id("login")).getText(), "Login: " + usuario);
	}


}
