package br.com.qualister.cenarios;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class CenarioDragAndDrop {

	private WebDriver driver;

	@Before
	public void iniciaTeste() {
		driver = new FirefoxDriver();
		driver.get("http://eliasnogueira.com/selenium/exercicios/brasil/webdriver/avancado");
		driver.findElement(By.partialLinkText("QuickLoja")).click();
	}

	@After
	public void finalizaTeste() {
		driver.quit();
	}

	@Test
	public void validarAdicaoRoupaCarrinho() {
		Actions action = new Actions(driver);

		//Page Object : PaginaLogin
		driver.findElement(By.id("usuariologin")).sendKeys("elias.nogueira");
		driver.findElement(By.id("usuariosenha")).sendKeys("123");
		driver.findElement(By.xpath("//button[text()='Entrar']")).click();

		//Page Object : PaginaMenu
		driver.findElement(By.partialLinkText("Gerenciar m�dulos")).click();
		driver.findElement(By.linkText("Produtos")).click();

		//Page Object : PaginaListagemProdutos
		driver.findElement(By.linkText("Novo produto")).click();

		//Page Object : PaginaProduto ou PaginaProdutoAbaPrincipal
		driver.findElement(By.linkText("Categorias")).click();

		//Page Object : PaginaProduto ou PaginaProdutoAbaCategorias
		WebElement roupas = driver.findElement(By.xpath("//li[contains(.,'Roupas')]"));
		action.clickAndHold(roupas).moveToElement(driver.findElement(By.id("selecionadas"))).release().build().perform();
	}
}
