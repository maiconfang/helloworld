package br.com.qualister.cenarios;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.thoughtworks.selenium.webdriven.JavascriptLibrary;

public class CenarioAutocomplete {

	private WebDriver driver;
	private WebDriverWait wait;

	@Before
	public void iniciaTeste() {
		driver = new FirefoxDriver();
		driver.get("http://eliasnogueira.com/selenium/exercicios/brasil/webdriver/avancado");
		driver.findElement(By.partialLinkText("Auto Completar")).click();
	}

	@After
	public void finalizaTeste() {
		driver.quit();
	}

	@Test
	public void validarAutoComplete() {
		wait = new WebDriverWait(driver, 15);

		driver.switchTo().frame("paginas");

		driver.findElement(By.id("estado_autocomplete")).sendKeys("Rio");
		By estado = By.xpath("//li[contains(.,'Rio Grande do Sul')]");
		wait.until(ExpectedConditions.presenceOfElementLocated(estado));
		driver.findElement(estado).click();

		driver.findElement(By.id("cidade_autocomplete")).sendKeys("Porto");
		By cidade = By.xpath("//li[contains(.,'Porto Alegre')]");
		wait.until(ExpectedConditions.presenceOfElementLocated(cidade));
		driver.findElement(cidade);
	}

	@Test
	public void validarProximoSemClicar() {
		wait = new WebDriverWait(driver, 15);

		driver.switchTo().frame("paginas");

		driver.findElement(By.id("estado_autocomplete")).sendKeys("Rio");
		By estado = By.xpath("//li[contains(.,'Rio Grande do Sul')]");
		wait.until(ExpectedConditions.presenceOfElementLocated(estado));
		driver.findElement(estado).click();

		driver.findElement(By.id("cidade_autocomplete")).sendKeys("Porto");
		By cidade = By.xpath("//li[contains(.,'Porto Alegre')]");
		wait.until(ExpectedConditions.presenceOfElementLocated(cidade));
		driver.findElement(cidade).click();

		((JavascriptExecutor) driver).executeScript("proximo();");

		driver.findElement(By.id("cep")).sendKeys("74823190");
		
		JavascriptLibrary js = new JavascriptLibrary();
		js.callEmbeddedSelenium(driver, "doFireEvent", driver.findElement(By.id("cep")), "blur");

	}
}
