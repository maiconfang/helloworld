package br.com.qualister.cenarios;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

public class CenarioActions {

	private WebDriver driver;

	@Before
	public void iniciaTeste() {
		driver = new FirefoxDriver();
		driver.get("http://eliasnogueira.com/selenium/exercicios/brasil/webdriver/avancado");
		driver.findElement(By.partialLinkText("Menu deslizante")).click();
	}

	@After
	public void finalizaTeste() {
		driver.quit();
	}

	@Test
	public void validarMenuDeslizante() {

		Actions action = new Actions(driver);

		action.moveToElement(driver.findElement(By.linkText("Treinamentos Qualister"))).build().perform();
		action.moveToElement(driver.findElement(By.linkText("Automa��o"))).build().perform();
		driver.findElement(By.linkText("Fundamentos")).click();

		driver.switchTo().frame("contentFrame");

		Assert.assertEquals("http://www.qualister.com.br/", driver.findElement(By.className("brand")).getAttribute("href"));
	}

}
