package br.com.qualister.cenarios;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import br.com.qualister.massa.FonteLogin;

public class CenarioDataDrivenTestNG {

	@BeforeTest
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		driver.get("http://eliasnogueira.com/selenium/exercicios/brasil/webdriver/avancado/");
		driver.findElement(By.partialLinkText("Login Data Driven")).click();
		wait = new WebDriverWait(driver, 15);
	}

	@AfterTest
	public void finalizaTeste() {
		driver.quit();
	}

	private WebDriver driver;
	private WebDriverWait wait;

	@Test(dataProvider = "usuarios_sucesso", dataProviderClass = FonteLogin.class)
	public void validaLogin(String usuario, int id) {
		driver.findElement(By.id("login")).sendKeys(usuario);
		driver.findElement(By.id("senha")).sendKeys(usuario);
		driver.findElement(By.xpath("//input[@value='Entrar']")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Sair")));
		try {
			Assert.assertEquals("Bem vindo Usuario 0" + id, driver.findElement(By.id("usuario")).getText());
			Assert.assertEquals("Login: " + usuario, driver.findElement(By.id("login")).getText());
		} finally {
			driver.findElement(By.xpath("//a[@href='sair.php']")).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("senha")));
		}
	}

	@Test(dataProvider = "usuarios_falha", dataProviderClass = FonteLogin.class)
	public void validaLoginComFalha(String usuario, int id) {
		driver.findElement(By.id("login")).clear();
		driver.findElement(By.id("login")).sendKeys(usuario);
		driver.findElement(By.id("senha")).clear();
		driver.findElement(By.id("senha")).sendKeys(usuario);
		driver.findElement(By.xpath("//input[@value='Entrar']")).click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("loader")));
		Assert.assertEquals("Login ou senha inválidos", driver.findElement(By.className("mensagem-erro")).getText());
	}

}