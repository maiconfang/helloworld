package br.com.qualister.cenarios;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;

public class CenarioTeclasAtalho {

	private WebDriver driver;

	@Before
	public void iniciaTeste() {
		driver = new FirefoxDriver();
		driver.get("http://eliasnogueira.com/selenium/exercicios/brasil/webdriver/avancado");
		driver.findElement(By.partialLinkText("Teclas de atalho")).click();
	}

	@After
	public void finalizaTeste() {
		driver.quit();
	}

	@Test
	public void validarExibicaoAlerta() {
		driver.findElement(By.tagName("body")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
		Assert.assertEquals(driver.switchTo().alert().getText(),"O alerta apareceu!");
	}

	@Test
	public void validarExibicaoConfirmacao() {
		driver.findElement(By.tagName("body")).sendKeys(Keys.chord(Keys.CONTROL, "c"));
		Assert.assertEquals(driver.switchTo().alert().getText(),"Esta � a confirma��o!");
	}

	@Test
	public void validarExibicaoPrompt() {
		driver.findElement(By.tagName("body")).sendKeys(Keys.chord(Keys.ALT, "p"));
		Assert.assertEquals(driver.switchTo().alert().getText(),"Digite seu nome:");
	}

	@Test
	public void validarExibicaoNovaJanela() {
		driver.findElement(By.tagName("body")).sendKeys(Keys.chord(Keys.ALT, "j"));
		Assert.assertEquals(driver.getTitle(),"Treinamento Selenium - Elias Nogueira");
		
	}
}
