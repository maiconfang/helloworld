package br.com.qualister.pageobject.cenario;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import br.com.qualister.quickloja.login.pageobject.PaginaLogin;

public class CenarioLogin {

	private WebDriver driver;

	@Before
	public void iniciaTeste() {
		driver = new FirefoxDriver();
		driver.get("http://eliasnogueira.com/selenium/exercicios/brasil/webdriver/avancado");
		driver.findElement(By.partialLinkText("QuickLoja")).click();
	}

	@After
	public void finalizaTeste() {
		driver.quit();
	}

	@Test
	public void testeLogin1() {
		PaginaLogin paginaLogin = new PaginaLogin();
		paginaLogin.preencherCampoLogin("elias.nogueira");
		paginaLogin.preencherCampoSenha("123456");
		paginaLogin.clicarEmEntrar();

		Assert.assertEquals("Usu�rio ou senha incorretos", paginaLogin.estUsuarioIncorreto());
	}

	@Test
	public void testeLogin2() {
		PaginaLogin paginaLogin = new PaginaLogin();
		paginaLogin.efetuarLogin("elias.nogueira", "12345");

		Assert.assertEquals("Usu�rio ou senha incorretos", paginaLogin.estUsuarioIncorreto());
	}
}
