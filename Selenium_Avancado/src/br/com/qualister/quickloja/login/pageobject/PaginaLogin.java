package br.com.qualister.quickloja.login.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.qualister.quickloja.core.WDSingleton;

public class PaginaLogin {

	private WebDriverWait wait;

	@FindBy(id = "usuariologin")
	private WebElement campoLogin;

	@FindBy(how = How.ID, using = "usuariosenha")
	private WebElement campoSenha;

	@FindBy(how = How.XPATH, using = "//button[text()='Entrar']")
	private WebElement botaoEntrar;

	@FindBy(how = How.CSS, using = ".alert.alert-error")
	private WebElement mensagemErro;

	public PaginaLogin() {
		wait = new WebDriverWait(WDSingleton.getDriver(), 15);
		PageFactory.initElements(WDSingleton.getDriver(), this);
	}

	public void preencherCampoLogin(String login) {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("usuariologin")));
		campoLogin.sendKeys(login);
	}

	public void preencherCampoSenha(String senha) {
		campoSenha.sendKeys(senha);
	}

	public void clicarEmEntrar() {
		botaoEntrar.click();
	}

	public String estUsuarioIncorreto() {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".alert.alert-error")));
		return mensagemErro.getText();
	}

	public void efetuarLogin(String usuario, String senha) {
		preencherCampoLogin(usuario);
		preencherCampoSenha(senha);
		clicarEmEntrar();
	}

}
