package br.com.qualister.quickloja.core.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.qualister.quickloja.core.WDSingleton;

public class PaginaMenu {

	public void acessarMenu(String menu) {
		String[] menus = menu.split("->");
		WebDriverWait wait = new WebDriverWait(WDSingleton.getDriver(), 10);
		for (String itemMenu : menus) {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText(itemMenu)));
			WDSingleton.getDriver().findElement(By.partialLinkText(itemMenu)).click();
		}
	}
}
