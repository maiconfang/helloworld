package br.com.qualister.quickloja.core;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.Set;
 
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
 
/**
 * Classe utilitaria para funcoes comuns
 * @author Elias Nogueira <elias.nogueira@qualister.com.br>
 *
 */
public class SeleniumUtils {
 
	private static DesiredCapabilities dc = null;
	
	/*
	 * Metodo utilizado para pegar o browser que esta configurado no arquivo 'configuracoes.properties' na pasta 'config'
	 */
	public static WebDriver pegaBrowser(String browser) {
		WebDriver driver = null;
		
		if (driver == null) {
			if (browser.toLowerCase().equals("firefox")) {
				driver = new FirefoxDriver();
			}
			
			if (browser.toLowerCase().equals("ie")) {
				DesiredCapabilities dc = DesiredCapabilities.internetExplorer();
				dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				System.setProperty("webdriver.ie.driver", pegaPropriedade("driver.ie"));
				
				driver = new InternetExplorerDriver(dc);
			}
			
			if (browser.toLowerCase().equals("googlechrome")) {
				DesiredCapabilities dc = DesiredCapabilities.chrome();
				dc.setJavascriptEnabled(true);
				dc.setCapability("chrome.binary", pegaPropriedade("chrome.instalacao"));
				System.setProperty("webdriver.chrome.driver", pegaPropriedade("driver.chrome"));
				
				driver = new ChromeDriver(dc);
			}
			
			if (browser.toLowerCase().equals("htmlunit")) {
				driver = new HtmlUnitDriver();
			}
		}
		return driver;
	}
	
	public static DesiredCapabilities getRemoteDrivers(String browser) {
		if (dc == null) {
		
			// se o browser for firefox instancia o driver do Firefox
			if (browser.toLowerCase().equals("firefox")) {
				DesiredCapabilities capabilities = DesiredCapabilities.firefox();
				capabilities.setPlatform(Platform.LINUX);
				
				dc = capabilities;
			}
 
			// se o browser for Google Chrome seta algumas configuracoes para execucao
			if (browser.toLowerCase().equals("googlechrome")) {
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				capabilities.setPlatform(Platform.LINUX);
				capabilities.setJavascriptEnabled(true);
				capabilities.setCapability("chrome.binary", pegaPropriedade("chrome.instalacao"));
				System.setProperty("webdriver.chrome.driver", pegaPropriedade("driver.chrome")); 
				
				dc = capabilities;
			}
			
			// se o browser for Internet Explorer configura a nao exibicao de warnings do IE
			if (browser.toLowerCase().equals("ie")) {
				File file = new File(pegaPropriedade("selenium.iedriver"));
				System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
				DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
				capabilities.setPlatform(Platform.LINUX);
				capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				
				dc = capabilities;
			}
		}
	return dc;
}	
	
	/*
	 * Metodo para tirar uma screenshot em formato png da tela.
	 * Basta apenas colocar o nome do arquivo. Lembre-se que arquivos de mesmo novo serao sobrescritos
	 */
	public static void capturaScreenshot(WebDriver driver, String nomeArquivo) {
		File arquivo = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
		try {
			FileUtils.copyFile(arquivo, new File("evidencias/" + nomeArquivo + ".png"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/*
	 * Metodo para encontrar uma janela aberta pela sua URL
	 */
	public static void encontraJanelaPelaURL(WebDriver driver, String url) {
		Set<String> janelas = driver.getWindowHandles();
		
		for (String janela : janelas) {
			driver.switchTo().window(janela);
			
			if (driver.getCurrentUrl().equals(url)) {
				break;
			}
		}
	}
	
	
	/*
	 * Metodo para encontrar uma janela aberta pelo seu titulo
	 */
	public static void encontraJanelaPeloTitulo(WebDriver driver, String tituloJanela) {
		Set<String> janelas = driver.getWindowHandles();
		
		for (String janela : janelas) {
			driver.switchTo().window(janela);
			
			if (driver.getTitle().equals(tituloJanela)) {
				break;
			}
		}
	}
	
	/*
	 * Metodo que pega uma propriedade existente no arquivo configuracoes.properties da pasta config e retorna o valor
	 */
	public static String pegaPropriedade(String nomePropriedade) {
		String valor = null;
		Properties propriedades = new Properties();
		
		try {
			propriedades.load(new FileInputStream("config/configuracao.properties"));
			valor = propriedades.getProperty(nomePropriedade);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return valor;
	}
	
}