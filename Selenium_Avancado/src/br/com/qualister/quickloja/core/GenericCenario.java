package br.com.qualister.quickloja.core;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public abstract class GenericCenario {

	public void capturarEvidencia(String nomeArquivo) {
		Date data = new Date();
		File arquivo = ((TakesScreenshot) WDSingleton.getDriver()).getScreenshotAs(OutputType.FILE);
		@SuppressWarnings("deprecation")
		String pastaEvidencias = "" + data.getYear() + data.getMonth() + data.getDay();
		try {
			FileUtils.copyFile(arquivo, new File("evidencias/" + pastaEvidencias + "/" + nomeArquivo + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
