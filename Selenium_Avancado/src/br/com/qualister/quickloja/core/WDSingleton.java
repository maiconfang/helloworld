package br.com.qualister.quickloja.core;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WDSingleton {

	private static WebDriver driver;

	public static WebDriver getDriver() {
		if (driver == null) {
			driver = new FirefoxDriver();
			// String url = "http://192.168.25.3:4441/wd/hub";
			// try {
			// driver = new RemoteWebDriver(new URL(url),
			// SeleniumUtils.getRemoteDrivers("firefox"));
			// } catch (MalformedURLException e) {
			// e.printStackTrace();
			// }
			driver.get("http://eliasnogueira.com/selenium/exercicios/brasil/webdriver/avancado");
		}
		return driver;
	}

	public static void acessaExercicio(String exercicio) {
		getDriver().findElement(By.partialLinkText(exercicio)).click();
	}

	public static void finalizar() {
		getDriver().quit();
		driver = null;
	}

}
