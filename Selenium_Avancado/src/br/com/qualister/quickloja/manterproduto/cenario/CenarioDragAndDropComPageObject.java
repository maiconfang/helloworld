package br.com.qualister.quickloja.manterproduto.cenario;

import org.junit.Test;
import org.testng.Assert;

import br.com.qualister.quickloja.core.GenericCenario;
import br.com.qualister.quickloja.core.WDSingleton;
import br.com.qualister.quickloja.core.pageobject.PaginaMenu;
import br.com.qualister.quickloja.login.pageobject.PaginaLogin;
import br.com.qualister.quickloja.manterproduto.pageobject.PaginaListagemProdutos;
import br.com.qualister.quickloja.manterproduto.pageobject.PaginaProduto;

public class CenarioDragAndDropComPageObject extends GenericCenario {

	@Test
	public void testeSelecaoCategoria() {
		WDSingleton.acessaExercicio("QuickLoja");
		PaginaLogin paginaLogin = new PaginaLogin();
		paginaLogin.preencherCampoLogin("elias.nogueira");
		paginaLogin.preencherCampoSenha("123");
		paginaLogin.clicarEmEntrar();
		PaginaMenu paginaMenu = new PaginaMenu();
		paginaMenu.acessarMenu("Gerenciar m�dulos->Produtos");
		PaginaListagemProdutos paginaListagemProdutos = new PaginaListagemProdutos();
		paginaListagemProdutos.solicitarCadastroProduto();
		PaginaProduto paginaProduto = new PaginaProduto();
		paginaProduto.acessarAba("Categorias");
		paginaProduto.selecionarCategoria("Roupas");
		paginaProduto.selecionarCategoria("Botas");
		WDSingleton.finalizar();
	}

	@Test
	public void testeSelecaoCategoria2() {
		try {
			WDSingleton.acessaExercicio("QuickLoja");
			PaginaLogin paginaLogin = new PaginaLogin();
			paginaLogin.preencherCampoLogin("elias.nogueira");
			paginaLogin.preencherCampoSenha("123");
			paginaLogin.clicarEmEntrar();
			PaginaMenu paginaMenu = new PaginaMenu();
			paginaMenu.acessarMenu("Gerenciar m�dulos->Produtos");
			PaginaListagemProdutos paginaListagemProdutos = new PaginaListagemProdutos();
			paginaListagemProdutos.solicitarCadastroProduto();
			PaginaProduto paginaProduto = new PaginaProduto();
			paginaProduto.acessarAba("Categoria");
			paginaProduto.selecionarCategoria("Roupas");
			paginaProduto.selecionarCategoria("Botas");
		} catch (Exception e) {
			capturarEvidencia("evidenciaErro01");
			Assert.fail(e.getMessage());
		} finally {
			WDSingleton.finalizar();
		}
	}
}
