package br.com.qualister.quickloja.manterproduto.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.qualister.quickloja.core.WDSingleton;

public class PaginaProduto {

	public void acessarAba(String nomeAba) {
		WebDriverWait wait = new WebDriverWait(WDSingleton.getDriver(), 10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(nomeAba)));
		WDSingleton.getDriver().findElement(By.linkText(nomeAba)).click();
	}

	public void selecionarCategoria(String nomeCategoria) {
		Actions action = new Actions(WDSingleton.getDriver());
		WebElement roupas = WDSingleton.getDriver().findElement(By.xpath("//li[contains(.,'" + nomeCategoria + "')]"));
		action.clickAndHold(roupas).moveToElement(WDSingleton.getDriver().findElement(By.id("selecionadas"))).release().build().perform();
	}
}