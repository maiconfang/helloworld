package br.com.qualister.massa;

import org.testng.annotations.DataProvider;

public class FonteLogin {

	@DataProvider(name = "usuarios_sucesso")
	public static Object[][] getUsuarioSucesso() {
		return new Object[][] { { "usuario01", 1 }, { "usuario02", 2 }, { "usuario03", 3 }, { "usuario04", 4 } };
	}
	
	@DataProvider(name = "usuarios_falha")
	public static Object[][] getUsuarioFalha() {
		return new Object[][] { { "xxxxx", 5 }, { "yyyyyyy", 2 } };
	}
	
	
}
